package com.y.springboot.spilder;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

public class FriefoxDriverSpilder {

    @Test
    public void friefoxSpilder() throws InterruptedException{
        FirefoxOptions options = new FirefoxOptions();
        options.setBinary("D:\\firefox\\firefox.exe");
        System.setProperty("webdriver.gecko.driver","E:\\springbootspider\\src\\main\\java\\com\\y\\springboot\\drivers\\geckodriver.exe");
        WebDriver driver = new FirefoxDriver(options);

        //通过driver控制浏览器打开链接
        String url = "https://agenthub.jetstar.com/newtradeloginagent.aspx?culture=zh-CN";
        driver.get(url);
        driver.manage().window().maximize();

        //登录
        driver.findElement(By.id("ControlGroupNewTradeLoginAgentView_AgentNewTradeLoginView_TextBoxUserID")).sendKeys("byqqan110");
        driver.findElement(By.id("ControlGroupNewTradeLoginAgentView_AgentNewTradeLoginView_PasswordFieldPassword")).sendKeys("Adidas@110");
//        test();
        driver.findElement(By.name("ControlGroupNewTradeLoginAgentView$AgentNewTradeLoginView$ButtonLogIn")).click();
        Thread.sleep(5000);

        //单程
        driver.findElement(By.id("ControlGroupTradeSalesHomeView_AvailabilitySearchInputTradeSalesHomeView_OneWay")).click();
        Thread.sleep(8000);

        //出发地
        driver.findElement(By.id("ControlGroupTradeSalesHomeView_AvailabilitySearchInputTradeSalesHomeView_TextBoxMarketOrigin1")).click();
        driver.findElement(By.cssSelector("a[data-city-code='PNH']")).click();
        Thread.sleep(2000);

        //目的地
        driver.findElement(By.xpath("//input[@id='ControlGroupTradeSalesHomeView_AvailabilitySearchInputTradeSalesHomeView_TextBoxMarketDestination1']/../span")).click();
        driver.findElement(By.cssSelector("a[data-city-code='HBA']")).click();
        Thread.sleep(7000);

        //选择时间
        driver.findElement(By.cssSelector("a.close.png-bg")).click();
        Thread.sleep(5000);
//        test();
        driver.findElement(By.id("ControlGroupTradeSalesHomeView_AvailabilitySearchInputTradeSalesHomeView_ButtonSubmit")).click();
    }
}
