package com.y.springboot.spilder;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Select;

import java.io.*;
import java.net.URLEncoder;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * https://www.cnblogs.com/davidwang456/articles/9143136.html
 * https://blog.csdn.net/xiaoliulang0324/article/details/79030752
 */
public class SeleniumhqSpilder {

    private String cookiePath = "E:\\springbootspider\\src\\main\\java\\com\\y\\springboot\\cookie.txt";

    @Test
    public void mockInput3() throws InterruptedException {
        //设置驱动程序路径
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\ideapad\\Downloads\\chromedriver2.33\\chromedriver.exe");
        //首选项
        HashMap<String, Object> opts = new HashMap<>();
        //禁止加载图片
        opts.put("profile.managed_default_content_settings.images", 2);
        //禁止浏览器弹窗
        opts.put("profile.default_content_setting_values.notifications", 2);

        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("prefs", opts);
        //关闭使用ChromeDriver打开浏览器时上部提示语"Chrome正在受到自动软件的控制"
//        options.addArguments("disable-infobars");
        //加载浏览器的静默模式，使浏览器在后台运行
//        options.addArguments("headless");

        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        // 将ACCEPT_SSL_CERTS变量设置为true,使证书有效
        capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);

        //新建一个谷歌浏览器对象（driver）
        WebDriver driver = new ChromeDriver(capabilities);

        //通过driver控制浏览器打开链接
        String url = "https://agenthub.jetstar.com/TradeSalesHome.aspx";
//        driver = a();
        driver.get(url);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().deleteAllCookies();

        File file = new File(cookiePath);
        try {
            Reader reader = new FileReader(file);
            char[] data = new char[1024];
            reader.read(data);
            System.out.println(new String(data));
            String cookies = new String(data);
            cookies = cookies.replace(";;", ";").replace("; ", ";").replace("\"", "");
            String[] cookiesArr = cookies.split(";");
            for (int i = 0; i < cookiesArr.length; i++) {
                if (!cookiesArr[i].contains("=")) {
//                cookiesArr[i] = cookiesArr[i] + "=" + null;
                    cookies.replace(cookiesArr[i], "");
                } else {
                    String[] cookieArr = cookiesArr[i].split("=", 2);
//                    System.out.println(cookieArr[0] + "--------" + cookieArr[1]);
                    Cookie cookie = new Cookie(cookieArr[0], URLEncoder.encode(cookieArr[1], "UTF-8"));
                    driver.manage().addCookie(cookie);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        Thread.sleep(5000);


        driver.navigate().refresh();

        //屏幕最大化
        //driver.manage().window().maximize();

        //登录
//        driver.findElement(By.id("ControlGroupNewTradeLoginAgentView_AgentNewTradeLoginView_TextBoxUserID")).sendKeys("byqqan110");
//        driver.findElement(By.id("ControlGroupNewTradeLoginAgentView_AgentNewTradeLoginView_PasswordFieldPassword")).sendKeys("Adidas@110");
//        driver.findElement(By.name("ControlGroupNewTradeLoginAgentView$AgentNewTradeLoginView$ButtonLogIn")).click();
//        Thread.sleep(5000);

        //单程
        driver.findElement(By.id("ControlGroupTradeSalesHomeView_AvailabilitySearchInputTradeSalesHomeView_OneWay")).click();
//        Thread.sleep(1000);

        //出发地   上海
        driver.findElement(By.id("ControlGroupTradeSalesHomeView_AvailabilitySearchInputTradeSalesHomeView_TextBoxMarketOrigin1")).click();
        driver.findElement(By.cssSelector("a[data-city-code='PVG']")).click();
        Thread.sleep(500);

        //目的地   东京
        driver.findElement(By.xpath("//input[@id='ControlGroupTradeSalesHomeView_AvailabilitySearchInputTradeSalesHomeView_TextBoxMarketDestination1']/../span")).click();
        driver.findElement(By.cssSelector("a[data-city-code='KIX|OSA']")).click();
//        Thread.sleep(1000);

        //选择时间  格式 日/月/年
        driver.findElement(By.id("ControlGroupTradeSalesHomeView_AvailabilitySearchInputTradeSalesHomeView_TextboxDepartureDate1")).clear();
        driver.findElement(By.id("ControlGroupTradeSalesHomeView_AvailabilitySearchInputTradeSalesHomeView_TextboxDepartureDate1")).sendKeys("14/03/2019");
        driver.findElement(By.id("ControlGroupTradeSalesHomeView_AvailabilitySearchInputTradeSalesHomeView_TextboxDepartureDate1")).sendKeys(Keys.ENTER);
        Thread.sleep(1000);

        //人数
        Select adults = new Select(driver.findElement(By.id("ControlGroupTradeSalesHomeView_AvailabilitySearchInputTradeSalesHomeView_DropDownListPassengerType_ADT")));
        adults.selectByValue("1");
        Select child = new Select(driver.findElement(By.id("ControlGroupTradeSalesHomeView_AvailabilitySearchInputTradeSalesHomeView_DropDownListPassengerType_CHD")));
        child.selectByValue("1");
        Select baby = new Select(driver.findElement(By.id("ControlGroupTradeSalesHomeView_AvailabilitySearchInputTradeSalesHomeView_DropDownListPassengerType_INFANT")));
        baby.selectByValue("1");

        //搜索航班
        driver.findElement(By.id("ControlGroupTradeSalesHomeView_AvailabilitySearchInputTradeSalesHomeView_ButtonSubmit")).click();
        Thread.sleep(1000);

        //获取价格
        int i = 2;
        String str = "";
//        //最后一个web元素对应的值
        String lastStr = driver.findElement(By.xpath("//*[@id='economy-PVG-KIX']/div[4]/div[last()-3]/div/div/div[1]/div[2]/div[2]/div[2]/div[2]/../input")).getAttribute("data-price-breakdown");
//        //判断是否为转乘
        String transferTo = driver.findElement(By.xpath("//*[@id='economy-PVG-KIX']/div[4]/div[last()-3]/div/div/div[1]/div[2]/div[2]/div[2]/div[2]/../input")).getAttribute("data-flightnumber");
        String[] transferToArr = transferTo.split("-");
        System.out.println("lastStr----------" + lastStr);
        //设置条件，不相等的时候值为true
        while (!(str.equals(lastStr))) {
            str = driver.findElement(By.xpath("//*[@id='economy-PVG-KIX']/div[4]/div[" + i + "]/div/div/div[1]/div[2]/div[2]/div[2]/div[2]/../input")).getAttribute("data-price-breakdown");
            JSONObject jsonObj = JSONObject.parseObject(str);
            //json中包含数组，先转为jsonarray
            JSONArray priceBreakdownArr = jsonObj.getJSONArray("PriceBreakdown");
            JSONObject adtObj = priceBreakdownArr.getJSONObject(0);
            //成人单价
            String adtPrice = adtObj.getString("PerPaxAmount");
            System.out.println(adtPrice);
            i++;
        }

        //选择班次   name = availability.MarketFareKeys[0]    data-departure-time 2:20   data-arrival-time  6:10   参数出发/到达时间需要修改
        driver.findElement(By.xpath("//input[@name='availability.MarketFareKeys[0]' and contains(@data-departure-time,'2:20') and contains(@data-arrival-time,'11:20')]/..")).click();

        //继续管理组合
        driver.findElement(By.id("submit_button")).click();
        Thread.sleep(1000);

        //继续管理行李
        driver.findElement(By.id("submit_button")).click();
        Thread.sleep(1000);

        //选择0公斤   继续管理座位
        driver.findElement(By.xpath("//i[@class='select-card__icon icon icon-baggage-0 baggage-tile__icon']/../../../..")).click();
        driver.findElement(By.cssSelector("button.button-component.theme-primary.size-large.is-expand.qa-continue")).click();
        Thread.sleep(1000);

        //无需行李
//        driver.findElement(By.cssSelector("button.button-component.theme-ghost.size-500")).click();
//        Thread.sleep(1000);

        //暂不选择座位
        driver.findElement(By.xpath("//*[@id='maincontent']/div[6]/section/div/div[2]/div[2]/div[2]/div/div[1]/div/div[3]/div/div[2]/button[1]")).click();
        Thread.sleep(1000);
        //若为转乘
        if (transferToArr.length > 1) {
            driver.findElement(By.xpath("//*[@id='maincontent']/div[6]/section/div/div[4]/div[2]/div[2]/div/div[1]/div/div[3]/div/div[2]/button[1]")).click();
            Thread.sleep(1000);
        }
        //继续管理额外项目
        driver.findElement(By.id("submit_button")).click();
        Thread.sleep(1000);

        //继续管理预定详情
        driver.findElement(By.id("submit_button")).click();
        Thread.sleep(1000);

        //继续检查并付款
        //姓
//        driver.findElement(By.id("passenger_Lastname_0")).sendKeys("ye");
//        //名
//        driver.findElement(By.id("passenger_Firstname_0")).sendKeys("ye");
//        //性别   1男  2女
//        Select sex = new Select(driver.findElement(By.id("passenger_Gender_0")));
//        sex.selectByValue("1");
//        //联络人
//        driver.findElement(By.id("js-contact_Name_Last")).sendKeys("ye");
//        driver.findElement(By.id("js-contact_Name_First")).sendKeys("ye");
//        driver.findElement(By.id("contact_Email_Address")).sendKeys("244892529@qq.com");
//        driver.findElement(By.id("contact_Phone_Number")).sendKeys("13685827647");
//        driver.findElement(By.id("submit_button")).click();


    }

    /**
     * 登录获取cookie
     *
     * @throws InterruptedException
     */
    @Test
    public void a() throws InterruptedException {
        System.out.println(new Date());
        //设置驱动程序路径
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\ideapad\\Downloads\\chromedriver2.33\\chromedriver.exe");
        //首选项
        HashMap<String, Object> opts = new HashMap<>();
        //禁止加载图片
        opts.put("profile.managed_default_content_settings.images", 2);
        opts.put("profile.managed_default_content_settings.css", 2);
        //禁止浏览器弹窗
        opts.put("profile.default_content_setting_values.notifications", 2);

        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("prefs", opts);
        //静默模式
        options.setHeadless(Boolean.TRUE);
        options.addArguments("--user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.78 Safari/537.36");

        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        // 将ACCEPT_SSL_CERTS变量设置为true,使证书有效
        capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);

        //新建一个谷歌浏览器对象（driver）
        WebDriver driver = new ChromeDriver(capabilities);

        //通过driver控制浏览器打开链接
        String url = "https://www.jetstar.com/hk/zh/home";
        //屏幕最大化
//        driver.manage().window().maximize();
        driver.get(url);
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);

        //写入文件
//        File write = new File(cookiePath);
//        OutputStream os = null;
//        try {
//            os = new FileOutputStream(write);
        String cookies = driver.manage().getCookieNamed("ak_bmsc").toString() + ";" + driver.manage().getCookieNamed("bm_sz").toString();
//            System.out.println(driver.manage().getCookieNamed("ak_bmsc").toString());
//            System.out.println(driver.manage().getCookieNamed("bm_sz").toString());

        Pattern pattern = Pattern.compile("bm_sz=[a-zA-Z\\d~/\\+=]*;|ak_bmsc=[a-zA-Z\\d~/\\+=]*;");
        Matcher matcher = pattern.matcher(cookies);
        String a = "";
        while (matcher.find()) {
            a += matcher.group(0);
        }
        System.out.println(a);
//            byte[] data = (driver.manage().getCookieNamed("ak_bmsc").toString() + driver.manage().getCookieNamed("bm_sz").toString()).getBytes();
//            os.write(data);
//            os.close();
//        } catch (IOException e) {
//            throw new RuntimeException("fail----------------------");
//        }
        System.out.println(new Date());
    }

    @Test
    public void tt() {
        File file = new File(cookiePath);
        try {
            Reader reader = new FileReader(file);
            char[] data = new char[1024];
            reader.read(data);
            System.out.println(new String(data));
            String cookies = new String(data);
            cookies = cookies.replace(";;", ";").replace("; ", ";").replace("\"", "");
            String[] cookiesArr = cookies.split(";");
            for (int i = 0; i < cookiesArr.length; i++) {
                if (!cookiesArr[i].contains("=")) {
//                cookiesArr[i] = cookiesArr[i] + "=" + null;
                    cookies.replace(cookiesArr[i], "");
                } else {
                    String[] cookieArr = cookiesArr[i].split("=", 2);
                    System.out.println(cookieArr[0] + "--------" + cookieArr[1]);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
