package com.y.springboot.spilder;

import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

/**
 * IE启动内核
 */
public class IEDriverSpilder {

    @Test
    public void ieDriver() throws InterruptedException{
        // 设置系统变量,并设置iedriver的路径为系统属性值
        System.setProperty("webdriver.ie.driver", "E:\\springbootspider\\src\\main\\java\\com\\y\\springboot\\drivers\\IEDriverServer.exe");
        //实例化 InternetExplorerDriver
        WebDriver driver = new InternetExplorerDriver();
    }
}
