package com.y.springboot.service.impl;

import com.y.springboot.model.Page;
import com.y.springboot.service.IDownload;
import com.y.springboot.utils.HttpUtil;

public class IDownloadImpl implements IDownload {

    @Override
    public Page download(String url) {
        Page page = new Page();
        String content = HttpUtil.getHttpContent(url);
        page.setUrl(url);
        page.setContent(content);
        return page;
    }
}
