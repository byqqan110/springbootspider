package com.y.springboot.service.impl;

import com.y.springboot.model.Page;
import com.y.springboot.service.IParser;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p class="detail">
 * 功能:网页数据解析
 * </p>
 *
 * @author yeqingqing
 * @ClassName Parser.
 * @Version V1.0.
 * @date 2019.01.10 15:14:56
 */
public class IParserImpl implements IParser {

    // log4j日志记录
    private Logger logger = LoggerFactory.getLogger(IParserImpl.class);

    @Override
    public void parser(Page page) {
        HtmlCleaner cleaner = new HtmlCleaner();
        /**
         * cleaner.clean()方法，如果page.getContent为null，那么整个程序就会一直阻塞在这里
         * 所以，在前面的代码中ISpider.start()方法，下载网页后，需要对内容进行判断，如果content为空，则跳过解析
         */
        TagNode node = cleaner.clean(page.getContent());
        //解析开始时间
        long start =System.currentTimeMillis();
        //解析商品
        if (page.getUrl().startsWith("https://item.jd.com/")){

        }
    }
}
