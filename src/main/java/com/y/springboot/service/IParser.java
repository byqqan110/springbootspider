package com.y.springboot.service;


import com.y.springboot.model.Page;

public interface IParser {

    public void parser(Page page);
}
