package com.y.springboot.service;

import com.y.springboot.model.Page;

/**
 * <p class="detail">
 * 功能:网页数据下载
 * </p>
 *
 * @author yeqingqing
 * @ClassName Download.
 * @Version V1.0.
 * @date 2019.01.10 15:10:43
 */
public interface IDownload {


    /**
     * <p class="detail">
     * 功能:下载给定url的网页数据
     * </p>
     *
     * @param url :
     * @return page
     * @author yeqingqing
     * @date 2019.01.10 15:10:43
     */
    public Page download(String url);

}
