package com.y.springboot;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.junit.Test;
import us.codecraft.xsoup.Xsoup;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TestSpilder {

    @Test
    public void testGet() throws IOException {
        //通过build模式创建httpclient对象
        CloseableHttpClient client = HttpClientBuilder.create().build();

        //构造代理主机
        HttpHost httpHost = new HttpHost("119.101.112.132", 9999, "https");
        //设置请求配置的代理地址
        RequestConfig requestConfig = RequestConfig.custom().setProxy(httpHost).build();

        HttpGet get = new HttpGet("https://www.jd.com");
        //设置请求的配置
        get.setConfig(requestConfig);

        //执行请求，返回响应
        CloseableHttpResponse response = client.execute(get);

        //取得响应的实体
        HttpEntity entity = response.getEntity();
        String str = EntityUtils.toString(entity);
        System.out.println(str);
    }

    @Test
    public void testPost() throws IOException {
        //通过build模式创建httpclient对象
        CloseableHttpClient client = HttpClientBuilder.create().build();

        //构造代理主机
//        HttpHost httpHost = new HttpHost("119.101.112.132", 9999, "https");
        //设置请求配置的代理地址
//        RequestConfig requestConfig = RequestConfig.custom().setProxy(httpHost).build();

        HttpPost post = new HttpPost("https://passport.csdn.net/login");

        List<NameValuePair> form = new ArrayList<NameValuePair>();
        form.add(new BasicNameValuePair("username", "244892529@qq.com"));
        form.add(new BasicNameValuePair("password", "adidas110"));
        UrlEncodedFormEntity entity = new UrlEncodedFormEntity(form, "UTF-8");

        //设置请求的配置
        post.setEntity(entity);

        //执行请求，返回响应
        CloseableHttpResponse response = client.execute(post);

        //取得响应的实体
        HttpEntity httpEntity = response.getEntity();
        String str = EntityUtils.toString(httpEntity);
        System.out.println(str);
    }

    public List<String> fetch(String url) throws IOException {
        //创建builder
        HttpClient client = HttpClientBuilder.create().build();
        //创建get请求
        HttpGet get = new HttpGet(url);
        //执行请求，并获得响应
        HttpResponse response = client.execute(get);
        //获得响应的实体数据
        HttpEntity entity = response.getEntity();
        //将实体转化为string
        String entityStr = EntityUtils.toString(entity);

        String xPath = "//a";
        List<String> c1s = Xsoup.select(entityStr, xPath).list();
        for (String c1 : c1s) {
            System.out.println(c1);
        }
        return c1s;
    }

    @Test
    public void fetchWeiBo() throws IOException {
        List<String> list = this.fetch("https://www.cnblogs.com/rookieCat/p/4680039.html");
        for (String a : list) {
            System.out.println(a);
        }
    }
}
