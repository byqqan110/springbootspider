package com.y.springboot.utils;

import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;

/**
 * <p class="detail">
 * 功能:
 * </p>
 *
 * @author yeqingqing
 * @ClassName Http util.
 * @Version V1.0.
 * @date 2019.01.10 14:37:26
 */
public class HttpUtil {
    // log4j日志记录，这里主要用于记录网页下载时间的信息
    private static Logger logger = LoggerFactory.getLogger(HttpUtil.class);
    // IP地址代理库Map
    private static Map<String, Integer> IPProxyRepository = new HashMap<>();
    private static String[] keysArray = null;   // keysArray是为了方便生成随机的代理对象

    static {
        InputStream in = HttpUtil.class.getResourceAsStream("IPProxyRepository.txt");
        InputStreamReader reader = new InputStreamReader(in);
        BufferedReader bReader = new BufferedReader(reader);
        String line = null;
        try {
            while ((line = bReader.readLine()) != null) {
                String[] spilt = line.split(":");
                IPProxyRepository.put(spilt[0], Integer.valueOf(spilt[1]));
            }
            Set<String> keys = IPProxyRepository.keySet();
            keysArray = keys.toArray(new String[keys.size()]);  // keysArray是为了方便生成随机的代理对象
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据url下载内容
     *
     * @param url the url
     * @return http content
     */
    public static String getHttpContent(String url) {
        CloseableHttpClient client = null;
        HttpHost httpHost = null;
        if (IPProxyRepository.size() > 0) {
            httpHost = getRandom();
            client = HttpClients.custom().setProxy(httpHost).build();
        } else {
            client = HttpClients.custom().build();
        }
        HttpGet request = new HttpGet(url); // 构建htttp get请求
        request.setHeader("user-agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.221 Safari/537.36 SE 2.X MetaSr 1.0");

        /**
         * 设置超时时间
         * setConnectTimeout：设置连接超时时间，单位毫秒。
         * setConnectionRequestTimeout：设置从connect Manager获取Connection 超时时间，单位毫秒。这个属性是新加的属性，因为目前版本是可以共享连接池的。
         * setSocketTimeout：请求获取数据的超时时间，单位毫秒。 如果访问一个接口，多少时间内无法返回数据，就直接放弃此次调用。
         */
        RequestConfig config = RequestConfig.custom().setConnectTimeout(5000).setConnectionRequestTimeout(1000).setSocketTimeout(2000).build();
        request.setConfig(config);

        String host = null;
        Integer port = null;
        if (httpHost != null){
            host = httpHost.getHostName();
            port = httpHost.getPort();
        }
        try {
            long start = System.currentTimeMillis();    //开始时间
            CloseableHttpResponse response = client.execute(request);
            long end = System.currentTimeMillis();      //结束时间
            logger.info("下载网页：{}，消耗时长：{} ms，代理信息：{}", url, end - start, host + ":" + port);
            return EntityUtils.toString(response.getEntity());
        } catch (IOException e) {
            logger.error("下载网页：{}出错，代理信息：{}，", url, host + ":" + port);
            // 如果该url为列表url，则将其添加到高优先级队列中
//            if (url.contains("list.jd.com") || url.contains("list.suning.com")) {   // 这里为硬编码
//                String domain = SpiderUtil.getTopDomain(url);   // jd.com
//                retryUrl(url, domain + SpiderConstants.SPIDER_DOMAIN_HIGHER_SUFFIX);    // 添加url到jd.com.higher中
//            }
            /**
             * 为什么要加入到高优先级队列中？
             * 如果该url为第一个种子url，但是解析却失败了，那么url仓库中会一直没有url，虽然爬虫程序还在执行，
             * 但是会一直提示没有url，这时就没有意义了，还需要尝试的另外一个原因是，下载网页失败很大可能是
             * 因为：
             *      1.此刻网络突然阻塞
             *      2.代理地址被限制，也就是被封了
             * 所以将其重新添加到高优先级队列中，再进行解析目前来说是比较不错的解决方案
             */
            e.printStackTrace();
        }


        return null;
    }

    /**
     * 随机获取代理ip
     *
     * @return random
     */
    public static HttpHost getRandom() {
        Random random = new Random();
        String host = keysArray[random.nextInt(keysArray.length)];
        int port = IPProxyRepository.get(host);
        HttpHost proxy = new HttpHost(host, port);
        return proxy;
    }
}
